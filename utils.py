import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np


#plot images
def plotImages(images_arr, cmap=None):
    fig, axes = plt.subplots(2, 11, figsize=(20,8))
    axes = axes.flatten()
    for img, ax in zip( images_arr, axes):
        ax.imshow(img, cmap=cmap)
        ax.axis('off')
    plt.tight_layout()
    plt.show()


 # This function will be used only in the notebook
def classDistribution(classLabels):
    '''
    Used to plot a histogram of sign classes
    
    parameters
    ----------
    classLabels     : list
                      a list contating all class labels for each sample
    '''
    # Count number of occurrences of each value
    classCount = np.bincount(classLabels)
    classes = np.arange(len(classCount))
    plt.figure(figsize=(10, 4))
    plt.bar(classes, classCount, 0.8, color='red', label='Inputs per Class')
    plt.xlabel('Class Number')
    plt.ylabel('Samples per Class')
    plt.title('Distribution of Training Samples Among Classes')
    plt.show()
    

def data_info(data_dir, train_dir, test_dir):
    '''
    Print information about the data to be used
    
    parameters
    ----------
    data_dir    : str
                  the data directory
    train_dir   : str
                  the training data directory
    test_dir    : str
                  the testing data directory
    '''
    data_dir = Path(data_dir)
    train_dir = Path(train_dir)
    test_dir = Path(test_dir)

    class_paths = [dI for dI in train_dir.iterdir() if dI.is_dir()]

    total_train = 0
    total_test = len(list(test_dir.iterdir())) - 1
    total_classes = len(class_paths)

    for class_path in class_paths:
      total_train += len(list(class_path.iterdir()))

    print('total classes', total_classes)
    print('total train',total_train)
    print('total test',total_test)
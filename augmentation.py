import numpy as np
import argparse
import os, os.path
import imutils
import cv2
import random
import csv
#type 1 - Meta     type 2 - Test    Type3 - Train
def augment(type,image,file,_list):
    angle=0
    for i in range (0,2):
        rotated_image = imutils.rotate(image, angle)
        angle=(angle+10)%360
        #crop - scale in % way from borders
        for j in range (0,4):
            h, w, _ = image.shape
            ha=int((random.randint(0,5)/100)*h)
            wa=int((random.randint(0,5)/100)*w)
            hz=int((random.randint(95,100)/100)*h)
            wz=int((random.randint(95,100)/100)*w)
            scaled_rotated_image=rotated_image[ha:hz,wa:wz]
            cv2.resize(scaled_rotated_image,(32,32))
            if(type==1):     
                cv2.imwrite( 'data/gtsrb-german-traffic-sign/augmented/meta/'+file.split('.')[0]+'_'+str(i)+'_'+str(j)+'.png', scaled_rotated_image)
                tmp_list=list();
                tmp_list.append(['meta/'+file.split('.')[0]+'_'+str(i)+'_'+str(j)+'.png',str(d[1]),str(d[2]),str(d[3]),str(d[4])])
                _list.extend(tmp_list);
            elif(type==2):
                cv2.imwrite( 'data/gtsrb-german-traffic-sign/augmented/test/'+file.split('.')[0]+'_'+str(i)+'_'+str(j)+'.png', scaled_rotated_image)            
                tmp_list=list();
                tmp_list.append(['test/'+file.split('.')[0]+'_'+str(i)+'_'+str(j)+'.png',str(d[6])])
                _list.extend(tmp_list);
            elif(type==3):
                cv2.imwrite( 'data/gtsrb-german-traffic-sign/augmented/train/'+d[7].split('/')[1]+'/'+file.split('.')[0]+'_'+str(i)+'_'+str(j)+'.png', scaled_rotated_image)
                tmp_list=list();
                tmp_list.append(['data/gtsrb-german-traffic-sign/augmented/train/'+d[7].split('/')[1]+'/'+file.split('.')[0]+'_'+str(i)+'_'+str(j)+'.png',str(d[6])])
                print("augment done")
                _list.extend(tmp_list);       
def load_csv(path):
    with open(path, newline='') as csvfile:
        return list(csv.reader(csvfile))    
def save_csv(path):
    with open(path, 'w') as newfile:
        for i in _list:
            wr = csv.writer(newfile, quoting=csv.QUOTE_ALL)
            wr.writerow(i)    
#check for required destination folders
if not os.path.exists("data/gtsrb-german-traffic-sign/augmented"):
    os.mkdir("data/gtsrb-german-traffic-sign/augmented")
if not os.path.exists("data/gtsrb-german-traffic-sign/augmented/train"):
    os.mkdir("data/gtsrb-german-traffic-sign/augmented/train/")
if not os.path.exists("data/gtsrb-german-traffic-sign/augmented/meta"):
    os.mkdir("data/gtsrb-german-traffic-sign/augmented/meta/")
if not os.path.exists("data/gtsrb-german-traffic-sign/augmented/test"):
    os.mkdir("data/gtsrb-german-traffic-sign/augmented/test/")
#meta folder
import sys
'''
data=load_csv('Meta.csv')
# #augmentation meta folder
_list=list()
for d in data[1:]:
    file=d[0].split('/')[1]
    image = cv2.imread(d[0])
    augment(1,image,file,_list)
save_csv("augmented/Meta_augmented.csv")
print("done meta")
'''
'''_list=list()
#test folders

data=load_csv('Test.csv')
for d in data[1:]:
    file=d[7].split('/')[1]
    image = cv2.imread(d[7])
    augment(2,image,file,_list)
save_csv("augmented/Test_augmented.csv")
print("done test")
'''

angle=0
#search number of images in each subfolder in array
# take max value
#roatet and scale depeding on number of subfolders
classes_images_number=[0]*43;
i=-2
for dirName, subdirList, fileList in os.walk('data/gtsrb-german-traffic-sign/Train/'):
    i=i+1
    classes_images_number[i]= len(fileList)
print(classes_images_number)
max_imgs=max(classes_images_number)
print(max_imgs)
classes_images_number=[0]*43;
#exit()
import sys
_list=list()
# #train folders
stop=1
i=0
j=0
import time
data=load_csv('data/gtsrb-german-traffic-sign/Train.csv')
for dirName, subdirList, fileList in os.walk("data/gtsrb-german-traffic-sign/Train/"):
    if stop==1: 
        stop=2
        for subd in subdirList:
            print("class",subd)
            #print(i)
            sys.stdout.flush()
            i=i+1
            if not os.path.exists("data/gtsrb-german-traffic-sign/augmented/train/"+str(subd)):
                print("class",subd)
                os.mkdir("data/gtsrb-german-traffic-sign/augmented/train/"+str(subd))
        for d in data[1:]:
            #print(d[6])
            #print(j)
            j=j+1
            file=d[7].split('/')[2]
            image = cv2.imread('data/gtsrb-german-traffic-sign/'+d[7])
            print(classes_images_number[int(d[7].split('/')[1])]," <? ",max_imgs)
            if (classes_images_number[int(d[7].split('/')[1])])<max_imgs:
                augment(3,image,file,_list)
                #print(d[7].split('/')[1])
                sys.stdout.flush()
                #time.sleep(4)
                classes_images_number[int(d[7].split('/')[1])]=classes_images_number[int(d[7].split('/')[1])]+8
            #print(classes_images_number)
save_csv("data/gtsrb-german-traffic-sign/augmented/Train_augmented.csv")